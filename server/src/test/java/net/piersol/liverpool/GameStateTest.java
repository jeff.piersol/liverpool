package net.piersol.liverpool;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.piersol.liverpool.types.Rank;
import net.piersol.liverpool.types.Suit;
import net.piersol.liverpool.util.IncrementalUUIDGenerator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author jeffp
 */
public class GameStateTest {

    private final UUID gameId;
    private final List<UUID> players;
    private final GameState state;
    private final IncrementalUUIDGenerator idGen;

    public GameStateTest() {
        gameId = IncrementalUUIDGenerator.generateFromLong(0);
        players = List.of(IncrementalUUIDGenerator.generateFromLong(1), IncrementalUUIDGenerator.generateFromLong(2));
        idGen = new IncrementalUUIDGenerator(3);
        state = new GameState(
                gameId,
                players,
                Stream.of(idGen.next(), idGen.next())
                        .flatMap(deckId -> {
                            String deckIdStr = deckId.toString();
                            return Stream.concat(
                                    // For each suit/rank combo, generate a card
                                    Stream.of(Suit.CLUB, Suit.DIAMOND, Suit.HEART, Suit.SPADE)
                                            .flatMap(suit -> Stream.of(Rank.values()).map(rank -> new CardEntity(idGen.next(), deckIdStr, suit, rank))),
                                    // Generate two jokers, distinguishing them with rank
                                    Stream.of(new CardEntity(idGen.next(), deckIdStr, Suit.JOKER, Rank.ACE), new CardEntity(idGen.next(), deckIdStr, Suit.JOKER, Rank.TWO)));
                        })
                        .collect(Collectors.toCollection(ArrayList::new)));
        System.out.println(state);
    }

    @BeforeAll
    public static void setUpClass() {

    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getCurrentPlayer method, of class GameState.
     */
    @Test
    public void testGetCurrentPlayer() {
        UUID expResult = players.get(0);
        UUID result = state.getCurrentPlayer();
        assertEquals(expResult, result);
    }

    /**
     * Test of isCurrentPlayer method, of class GameState.
     */
    @Test
    public void testIsCurrentPlayer() {
        boolean expResult = false;
        boolean result = state.isCurrentPlayer(players.get(1));
        assertEquals(expResult, result);
    }

    /**
     * Test of hasCurrentPlayerDrawn method, of class GameState.
     */
    @Test
    public void testHasCurrentPlayerDrawn() {
        boolean expResult = false;
        boolean result = state.hasCurrentPlayerDrawn();
        assertEquals(expResult, result);
    }

//    /**
//     * Test of getCurrentPlayerNotifier method, of class GameState.
//     */
//    @Test
//    public void testGetCurrentPlayerNotifier() {
//        System.out.println("getCurrentPlayerNotifier");
//        GameState instance = null;
//        Flux<UUID> expResult = null;
//        Flux<UUID> result = instance.getCurrentPlayerNotifier();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getHand method, of class GameState.
//     */
//    @Test
//    public void testGetHand() {
//        System.out.println("getHand");
//        UUID playerId = null;
//        GameState instance = null;
//        List<CardEntity> expResult = null;
//        List<CardEntity> result = instance.getHand(playerId);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getSets method, of class GameState.
//     */
//    @Test
//    public void testGetSets() {
//        System.out.println("getSets");
//        GameState instance = null;
//        Map<UUID, List<CardEntity>> expResult = null;
//        Map<UUID, List<CardEntity>> result = instance.getSets();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getLiveDiscard method, of class GameState.
//     */
//    @Test
//    public void testGetLiveDiscard() {
//        System.out.println("getLiveDiscard");
//        GameState instance = null;
//        CardEntity expResult = null;
//        CardEntity result = instance.getLiveDiscard();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of draw method, of class GameState.
//     */
//    @Test
//    public void testDraw() {
//        System.out.println("draw");
//        UUID playerId = null;
//        boolean fromDiscard = false;
//        GameState instance = null;
//        CardEntity expResult = null;
//        CardEntity result = instance.draw(playerId, fromDiscard);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of discard method, of class GameState.
//     */
//    @Test
//    public void testDiscard() {
//        System.out.println("discard");
//        UUID playerId = null;
//        UUID cardId = null;
//        GameState instance = null;
//        boolean expResult = false;
//        boolean result = instance.discard(playerId, cardId);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of main method, of class GameState.
//     */
//    @Test
//    public void testMain() {
//        System.out.println("main");
//        String[] args = null;
//        GameState.main(args);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
}
