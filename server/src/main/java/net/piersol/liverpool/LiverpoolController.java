package net.piersol.liverpool;

import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.DgsDataFetchingEnvironment;
import com.netflix.graphql.dgs.DgsMutation;
import com.netflix.graphql.dgs.DgsQuery;
import com.netflix.graphql.dgs.DgsSubscription;
import com.netflix.graphql.dgs.InputArgument;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import net.piersol.liverpool.DgsConstants.GAME;
import net.piersol.liverpool.types.Card;
import net.piersol.liverpool.types.Game;
import net.piersol.liverpool.types.GameInput;
import net.piersol.liverpool.types.Player;
import org.reactivestreams.Publisher;

@DgsComponent
public class LiverpoolController {

    private final LiverpoolEngine service;
    private final CardMapper cardMapper;

    public LiverpoolController(LiverpoolEngine service, CardMapper cardMapper) {
        this.service = service;
        this.cardMapper = cardMapper;
    }

    @DgsQuery
    public Game game(@InputArgument("id") String id) {
        GameState gameState = service.getGames(UUID.fromString(id)).get(0);
        List<Player> players = gameState.players.stream().map(playerId -> new Player(playerId.toString(), "name here")).collect(Collectors.toList());
        return new Game(gameState.id.toString(), players, null, null, null, null, null, false);
    }

    @DgsQuery
    public List<Game> games() {
        return service.getGames().stream()
                .map(gameState -> {
                    List<Player> players = gameState.players.stream().map(playerId -> new Player(playerId.toString(), "name here")).collect(Collectors.toList());
                    return new Game(gameState.id.toString(), players, null, null, null, null, null, false);
                })
                .collect(Collectors.toList());
    }

    @DgsMutation
    public Game createGame(@InputArgument("gameSpec") GameInput gameSpec) {
        List<UUID> playerIds = gameSpec.getPlayers().stream()
                .map(UUID::fromString)
                .collect(Collectors.toList());
        GameState gameState = service.createGame(playerIds);
        List<Player> players = createPlayers(gameSpec.getPlayers());
        return new Game(gameState.id.toString(), players, null, new ArrayList(gameState.getSets().values()), new ArrayList<>(), null, players.get(0), false);
    }

    @DgsData(parentType = GAME.TYPE_NAME)
    public List<Card> hand(DgsDataFetchingEnvironment dfe, @InputArgument("player") String playerId) {
        //TODO some auth here
        Game game = dfe.getSource();
        UUID gameId = UUID.fromString(game.getId());
        return cardMapper.toDTOs(service.getHand(gameId, UUID.fromString(playerId)));
    }

    @DgsData(parentType = GAME.TYPE_NAME)
    public List<Card> sets(DgsDataFetchingEnvironment dfe) {
        return Collections.emptyList();
    }

    @DgsData(parentType = GAME.TYPE_NAME)
    public List<Card> runs(DgsDataFetchingEnvironment dfe) {
        return Collections.emptyList();
    }

    @DgsData(parentType = GAME.TYPE_NAME)
    public Card liveDiscard(DgsDataFetchingEnvironment dfe) {
        Game game = dfe.getSource();
        UUID gameId = UUID.fromString(game.getId());
        return cardMapper.toDTO(service.getLiveDiscard(gameId));
    }

    @DgsData(parentType = GAME.TYPE_NAME)
    public Player currentPlayer(DgsDataFetchingEnvironment dfe) {
        Game game = dfe.getSource();
        UUID gameId = UUID.fromString(game.getId());
        return new Player(service.getGame(gameId).getCurrentPlayer().toString(), "name me");
    }

    @DgsData(parentType = GAME.TYPE_NAME)
    public boolean hasCurrentPlayerDrawn(DgsDataFetchingEnvironment dfe) {
        Game game = dfe.getSource();
        UUID gameId = UUID.fromString(game.getId());
        return service.getGame(gameId).hasCurrentPlayerDrawn();
    }

    @DgsSubscription
    public Publisher<String> nextTurn(@InputArgument("game") String gameId) {
        return service.getCurrentPlayerFlux(UUID.fromString(gameId)).map(UUID::toString);
    }

    //////////////// Game Mutations ////////////////
    @DgsMutation
    public Card draw(@InputArgument("game") String gameId, @InputArgument("player") String playerId, boolean fromDiscard) {
        UUID gameUuid = UUID.fromString(gameId);
        UUID playerUuid = UUID.fromString(playerId);
        return cardMapper.toDTO(service.draw(gameUuid, playerUuid, fromDiscard));
    }

    @DgsMutation
    public boolean discard(@InputArgument("game") String gameId, @InputArgument("player") String playerId, @InputArgument("card") String cardId) {
        UUID gameUuid = UUID.fromString(gameId);
        UUID playerUuid = UUID.fromString(playerId);
        UUID cardUuid = UUID.fromString(cardId);
        return service.getGame(gameUuid).discard(playerUuid, cardUuid);
    }

    public static List<Player> createPlayers(String... playerIds) {
        return createPlayers(Arrays.asList(playerIds));
    }

    public static List<Player> createPlayers(Collection<String> playerIds) {
        AtomicInteger i = new AtomicInteger(); // Shouldn't really be using state here, but eh
        return playerIds.stream()
                .map(playerId -> new Player(playerId, "Player " + i.incrementAndGet()))
                .collect(Collectors.toList());
    }
}
