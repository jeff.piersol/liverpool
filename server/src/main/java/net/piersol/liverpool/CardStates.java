package net.piersol.liverpool;

/**
 * The possible states of a card.
 *
 * @author jeffp
 */
public enum CardStates {
    /**
     * The card is in the draw pile and has not yet been seen by any player.
     */
    DRAW_PILE,
    /**
     * The card is in a player's hand.
     */
    IN_PLAY,
    /**
     * The card is part of a set.
     */
    SET,
    /**
     * The card is part of a run.
     */
    RUN,
    /**
     * The card is currently the discard.
     */
    DISCARD,
    /**
     * The card is buried under the current discard.
     */
    DEAD
}
