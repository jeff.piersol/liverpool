package net.piersol.liverpool;

import net.piersol.liverpool.util.Util;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

/**
 *
 * @author jeffp
 */
public class GameState {

    /**
     * The ID of this game.
     */
    public final UUID id;

    /**
     * The players in the game.
     */
    public final List<UUID> players;

    /**
     * The current player. Can be null.
     */
    private UUID currentPlayer;

    /**
     * Whether or not the current player has drawn yet.
     */
    private boolean hasCurrentPlayerDrawn;

    /**
     * The current player notifier.
     */
    private final Sinks.Many<UUID> currentPlayerSink = Sinks.many().replay().latest();

    /**
     * The cards in the game. This list should never be reordered, but it can be
     * appended to with a new deck.
     */
    private final List<CardEntity> cards;

    /**
     * The state of each card in the game, indexed by card ID.
     */
    private final Map<UUID, CardState> cardStates;

    public GameState(final Collection<UUID> playerIds) {
        this(playerIds, 2);
    }

    private GameState(final Collection<UUID> playerIds, final int numInitialDecks) {
        this(UUID.randomUUID(), List.copyOf(playerIds), Util.generateDecks(numInitialDecks));
    }

    /**
     * Lower-level constructor for higher-level constructors and unit tests.
     *
     * @param gameId
     * @param players
     * @param cards
     */
    protected GameState(final UUID gameId, final List<UUID> players, final List<CardEntity> cards) {
        this.id = gameId;
        this.players = players;
        this.cards = cards;
        this.cardStates = cards.stream().collect(Collectors.toMap(CardEntity::getId, card -> CardState.drawCard()));
        
        // Shuffle cards (Card IDs should be random)
        cards.sort(Comparator.comparing(CardEntity::getId));

        // Deal
        //TODO check invariants (e.g., enough cards for #players)
        for (UUID playerId : players) {
            for (int i = 0; i < 11; i++) {
                cardStates.put(getNextDrawCardId(), CardState.inPlay(playerId));
            }
        }

        cardStates.put(getNextDrawCardId(), CardState.discard());
        advanceCurrentPlayer();
    }

    //////////////// Queries ////////////////
    /**
     * Get the next available draw card.
     *
     * @return
     */
    private CardEntity getNextDrawCard() {
        return cards.stream()
                .filter(card -> cardStates.get(card.getId()).getState() == CardStates.DRAW_PILE)
                .findFirst().orElseThrow(() -> new NoSuchElementException("need to add new deck!"));
        // TODO add another deck if no draw cards
    }

    private UUID getNextDrawCardId() {
        return getNextDrawCard().getId();
    }

    /**
     * Get the player whose turn it is.
     *
     * @return
     */
    public UUID getCurrentPlayer() {
        return currentPlayer;
    }

    /**
     * Whether or not the given player is the current player.
     *
     * @param playerId
     * @return
     */
    public boolean isCurrentPlayer(UUID playerId) {
        return Objects.equals(currentPlayer, playerId);
    }

    /**
     * Whether or not the current player has drawn yet.
     *
     * @return
     */
    public boolean hasCurrentPlayerDrawn() {
        return hasCurrentPlayerDrawn;
    }

    public Flux<UUID> getCurrentPlayerNotifier() {
        return currentPlayerSink.asFlux();
    }

    /**
     * Given a player, get the cards in their hand.
     *
     * @param playerId
     * @return
     */
    public List<CardEntity> getHand(UUID playerId) {
        return cards.stream()
                .filter(card -> {
                    CardState state = cardStates.get(card.getId());
                    return state.getState() == CardStates.IN_PLAY && state.getStateId().equals(playerId);
                })
                .collect(Collectors.toList());
    }

    /**
     * SetID -> cards in set
     *
     * @return
     */
    public Map<UUID, List<CardEntity>> getSets() {
        //TODO does this even work
        return cardStates.entrySet().stream()
                .filter(cardIdAndCardState -> cardIdAndCardState.getValue().getState().equals(CardStates.SET))
                .collect(Collectors.groupingBy(cardIdAndCardState -> cardIdAndCardState.getValue().getStateId(),
                        Collectors.mapping(cardIdAndCardState -> cards.stream().filter(card -> card.getId().equals(cardIdAndCardState.getKey())).findAny().get(), Collectors.toList())));
    }

    public CardEntity getLiveDiscard() {
        return cards.stream()
                .filter(card -> {
                    CardState state = cardStates.get(card.getId());
                    return state.getState() == CardStates.DISCARD;
                })
                .findAny().orElse(null);
    }
    public static GameState EMPTY_GAME = new GameState();

    //////////////// Mutations ////////////////
    private Iterator<UUID> currentPlayerTracker = Collections.emptyIterator();

    private void advanceCurrentPlayer() {
        // Restart iteration at first player if no more available in current iterator
        if (!currentPlayerTracker.hasNext()) {
            currentPlayerTracker = players.iterator();
        }
        currentPlayer = currentPlayerTracker.next();
        hasCurrentPlayerDrawn = false;
        currentPlayerSink.emitNext(currentPlayer, Sinks.EmitFailureHandler.FAIL_FAST);
    }

    public CardEntity draw(UUID playerId, boolean fromDiscard) {
        if (!isCurrentPlayer(playerId) || !players.contains(playerId) || hasCurrentPlayerDrawn) {
            return null;
        }

        CardEntity drawCard = null;
        if (fromDiscard) {
            drawCard = getLiveDiscard();
        }
        if (drawCard == null) {
            drawCard = getNextDrawCard();
        }
        cardStates.put(drawCard.getId(), CardState.inPlay(playerId));
        hasCurrentPlayerDrawn = true;
        return drawCard;
    }

    public boolean discard(UUID playerId, UUID cardId) {
        CardState cardState = cardStates.get(cardId);
        // Abort on error conditions
        if (!isCurrentPlayer(playerId) || !hasCurrentPlayerDrawn || !players.contains(playerId) || cardState == null) {
            return false;
        }

        // Allow this discard as long as the player currently has it in his hand
        boolean doDiscard = cardState.getState().equals(CardStates.IN_PLAY) && cardState.getStateId().equals(playerId);
        if (doDiscard) {
            // Bury the current discard, if it exists
            CardEntity currentDiscard = getLiveDiscard();
            if (currentDiscard != null) {
                cardStates.put(currentDiscard.getId(), CardState.dead());
            }
            // Move the player's card into the discard pile
            CardState prevValue = cardStates.put(cardId, CardState.discard());
            if (prevValue == null) {
                throw new RuntimeException("Discarded a card that was not previously in the game.");
            }
            // Next player's turn
            advanceCurrentPlayer();
        }
        return doDiscard;
    }

    @Override
    public String toString() {
        return Stream.concat(Stream.of(
                "               Card ID               |                 Deck                 |  Suit   | Rank  | State",
                "-------------------------------------|--------------------------------------|---------|-------|--------"), cards.stream()
                        .map(card -> String.format("%s | %s | %7s | %5s | %s", card.getId(), card.getDeck(), card.getSuit(), card.getRank(), cardStates.get(card.getId()))))
                .collect(Collectors.joining("\n"));
    }

    //////////////// Special ////////////////
    /**
     * Only used to initialize the empty game.
     */
    private GameState() {
        this.id = new UUID(0, 0);
        this.players = Collections.emptyList();
        this.cards = Collections.emptyList();
        this.cardStates = Collections.emptyMap();
    }

    public static void main(String[] args) {
        GameState gs = new GameState(List.of(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID()));
//        System.out.println(gs.cards);
//        System.out.println(gs.cardStates);
        System.out.println(gs);
    }
}
