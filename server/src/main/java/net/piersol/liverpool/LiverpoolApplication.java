package net.piersol.liverpool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class LiverpoolApplication {

    @Bean
    WebMvcConfigurer corsConfigurer() {
        return new CorsConfigurer();
    }

    public static void main(String[] args) {
        SpringApplication.run(LiverpoolApplication.class, args);
    }

}
