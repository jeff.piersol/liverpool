package net.piersol.liverpool;

import java.util.List;
import java.util.UUID;
import net.piersol.liverpool.types.Card;
import org.mapstruct.Mapper;

/**
 *
 * @author jeffp
 */
@Mapper(componentModel = "spring", imports = UUID.class)
public interface CardMapper {

    Card toDTO(CardEntity entity);

    CardEntity toEntity(Card dto);

    List<Card> toDTOs(List<CardEntity> entities);

    List<CardEntity> toEntity(List<Card> dtos);

    default String uuidToString(UUID uuid) {
        return uuid.toString();
    }

    default UUID stringToUuid(String uuid) {
        return UUID.fromString(uuid);
    }
}
