package net.piersol.liverpool;

import java.util.Objects;
import java.util.UUID;
import net.piersol.liverpool.types.Rank;
import net.piersol.liverpool.types.Suit;

/**
 *
 * @author jeffp
 */
public class CardEntity {

    private UUID id;

    private String deck;

    private Suit suit;

    private Rank rank;

    public CardEntity() {
    }

    public CardEntity(UUID id, String deck, Suit suit, Rank rank) {
        this.id = id;
        this.deck = deck;
        this.suit = suit;
        this.rank = rank;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDeck() {
        return deck;
    }

    public void setDeck(String deck) {
        this.deck = deck;
    }

    public Suit getSuit() {
        return suit;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "Card{" + "id='" + id + "'," + "deck='" + deck + "'," + "suit='" + suit + "'," + "rank='" + rank + "'" + "}";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CardEntity other = (CardEntity) obj;
        if (!Objects.equals(this.deck, other.deck)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (this.suit != other.suit) {
            return false;
        }
        if (this.rank != other.rank) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(id, deck, suit, rank);
    }
}
