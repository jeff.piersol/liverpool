package net.piersol.liverpool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

/**
 *
 */
@Service
public class LiverpoolEngine {

    private final Map<UUID, GameState> games = new HashMap<>();

    public GameState getGame(UUID gameId) {
        return games.getOrDefault(gameId, GameState.EMPTY_GAME);
    }

    /**
     * Get the games corresponding to the given IDs, or all games if no IDs
     * given.
     *
     * @param gameIds
     * @return a list of the same size as input, with {@code null} entries for
     * IDs not found
     */
    public List<GameState> getGames(UUID... gameIds) {
        return gameIds.length > 0
                ? Arrays.stream(gameIds)
                        .map(games::get)
                        .collect(Collectors.toList())
                : new ArrayList<>(games.values());
    }

    public GameState createGame(Collection<UUID> playerIds) {
        //TODO validate >0 players
        GameState game = new GameState(playerIds);
        games.put(game.id, game);
        return game;
    }

    public List<CardEntity> getHand(UUID gameId, UUID playerId) {
        GameState gameState = getGame(gameId);
        return gameState.getHand(playerId);
    }

//    public List<Card> getSets(UUID gameId) {
//        GameState gameState = getGame(gameId);
//        return gameState.getSets();
//    }
    public CardEntity getLiveDiscard(UUID gameId) {
        GameState gameState = getGame(gameId);
        return gameState.getLiveDiscard();
    }

    public UUID getCurrentPlayer(UUID gameId) {
        GameState gameState = getGame(gameId);
        return gameState.getCurrentPlayer();
    }

    public boolean hasCurrentPlayerDrawn(UUID gameId) {
        GameState gameState = getGame(gameId);
        return gameState.hasCurrentPlayerDrawn();
    }

    public Flux<UUID> getCurrentPlayerFlux(UUID gameId) {
        GameState gameState = getGame(gameId);
        return gameState.getCurrentPlayerNotifier();
    }

    public CardEntity draw(UUID gameId, UUID playerId, boolean fromDiscard) {
        GameState gameState = getGame(gameId);
        return gameState.draw(playerId, fromDiscard);
    }
}
