package net.piersol.liverpool.util;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import net.piersol.liverpool.CardEntity;
import net.piersol.liverpool.types.Rank;
import net.piersol.liverpool.types.Suit;

/**
 *
 * @author jeffp
 */
public class Util {

    /**
     * Generate 54-card decks.
     *
     * @param numDecks the number of decks to generate
     * @return
     */
    public static List<CardEntity> generateDecks(final int numDecks) {
        return IntStream.range(0, numDecks)
                .mapToObj(i -> UUID.randomUUID())
                // For each deck, generate its cards and flat map them into one pile
                .flatMap(deckId -> {
                    String deckIdStr = deckId.toString();
                    return Stream.concat(
                            // For each suit/rank combo, generate a card
                            Stream.of(Suit.CLUB, Suit.DIAMOND, Suit.HEART, Suit.SPADE)
                                    .flatMap(suit -> Stream.of(Rank.values()).map(rank -> new CardEntity(UUID.randomUUID(), deckIdStr, suit, rank))),
                            // Generate two jokers, distinguishing them with rank
                            Stream.of(new CardEntity(UUID.randomUUID(), deckIdStr, Suit.JOKER, Rank.ACE), new CardEntity(UUID.randomUUID(), deckIdStr, Suit.JOKER, Rank.TWO)));
                })
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
