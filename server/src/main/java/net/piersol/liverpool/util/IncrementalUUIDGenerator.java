package net.piersol.liverpool.util;

import java.util.UUID;

/**
 * Generates a new ID incrementally, likely not globally unique. Only works
 * properly up to {@link Long.MAX_VALUE} calls to {@link next()}.
 */
public class IncrementalUUIDGenerator {

    private long curVal;

    public IncrementalUUIDGenerator() {
        this(0);
    }

    public IncrementalUUIDGenerator(long initialValue) {
        curVal = initialValue;
    }

    /**
     * Set the next value that the generator will output.
     *
     * @param value
     * @return the current "next value"
     */
    public long setNextValue(long value) {
        long oldValue = curVal;
        curVal = value;
        return oldValue;
    }

    public UUID next() {
        return generateFromLong(curVal++);
    }

    public static UUID generateFromLong(long l) {
        String str = String.format("%032x", l);
        str = String.format("%s-%s-%s-%s-%s", str.substring(0, 8), str.substring(8, 12), str.substring(12, 16), str.substring(16, 20), str.substring(20));
        return UUID.fromString(str);
    }

    public static void main(String[] args) {
        IncrementalUUIDGenerator idGen = new IncrementalUUIDGenerator();
        for (int i = 0; i < 100; i++) {
            System.out.println(idGen.next());
        }
    }
}
