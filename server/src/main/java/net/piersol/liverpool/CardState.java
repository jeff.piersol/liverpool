package net.piersol.liverpool;

import java.util.Objects;
import java.util.UUID;

/**
 * A value-based object that completely specifies the state of a card in a game.
 *
 * @author jeffp
 */
public class CardState {

    /**
     * The state that the card is in.
     */
    private final CardStates state;
    /**
     * The ID associated with the card state, if applicable. For a card that is
     * {@link CardStates.IN_PLAY}, this is the player ID. For a card that is
     * {@link CardStates.SET} or {@link CardStates.RUN}, this is the ID of the
     * set or run it belongs to.
     */
    private final UUID stateId;

    private CardState(CardStates state, UUID stateId) {
        this.state = state;
        this.stateId = stateId;
    }

    public static CardState drawCard() {
        return new CardState(CardStates.DRAW_PILE, null);
    }

    /**
     *
     * @param playerId the ID of the player who has this card in their hand
     * @return
     */
    public static CardState inPlay(UUID playerId) {
        Objects.requireNonNull(playerId);
        return new CardState(CardStates.IN_PLAY, playerId);
    }

    public static CardState discard() {
        return new CardState(CardStates.DISCARD, null);
    }

    public static CardState dead() {
        return new CardState(CardStates.DEAD, null);
    }

    /**
     *
     * @param setId the unique ID of the set that the card belongs to
     * @return
     */
    public static CardState set(UUID setId) {
        Objects.requireNonNull(setId);
        return new CardState(CardStates.SET, setId);
    }

    /**
     *
     * @param runId the unique ID of the set that the card belongs to
     * @return
     */
    public static CardState run(UUID runId) {
        Objects.requireNonNull(runId);
        return new CardState(CardStates.RUN, runId);
    }

    @Override
    public String toString() {
        return "CardState{" + "state=" + state + ", stateId=" + stateId + '}';
    }

    public CardStates getState() {
        return state;
    }

    public UUID getStateId() {
        return stateId;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.state);
        hash = 47 * hash + Objects.hashCode(this.stateId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final CardState other = (CardState) obj;
        return this.state == other.state && Objects.equals(this.stateId, other.stateId);
    }
}
