# Liverpool!

## Building

From the appropriate directories, run these commands in order:

```bash
# Start backend
gradle bootRun
# Generate client types from GQL schema
npm run apollo:downloadSchema && npm run apollo:generate
# Run the UI
npm start
```

Then use [GraphiQL](http://localhost:8080/graphiql) to create a game:

```graphql
mutation {
  createGame(gameSpec: {players: ["debac1ed-7871-8a9b-9e7b-2ecb97349d92", "debac1ed-7871-8a9b-9e7b-2ecb97349d93"]}) {
    id
    hand(player: "debac1ed-7871-8a9b-9e7b-2ecb97349d92") {
      id
      deck
      suit
      rank
    }
  }
}
```

The UI should show a hand.
