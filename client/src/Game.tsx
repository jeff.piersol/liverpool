import { gql, useQuery } from "@apollo/client";
import { allGamesWithPlayers } from "./generated/allGamesWithPlayers";
import { PlayerHand } from './PlayerHand';
import { DrawPile } from './DrawPile';
import { DiscardPile } from './DiscardPile';

export const ALL_GAMES = gql`
query allGamesWithPlayers {
    games {
        id
        players {
            id
        }
    }
}
`;

/**
 * Uniquely identifies a player's viewpoint among all games and players.
 */
export interface GameViewId {
  gameId: string
  playerId: string
}

export function Game({ gameId, playerId }: Partial<GameViewId>) {
  const { data } = useQuery<allGamesWithPlayers>(ALL_GAMES);
  const game0 = data?.games[0];
  const game0players = game0?.players || [{ id: "" }];
  gameId = gameId || game0?.id;
  playerId = playerId || game0players[0].id;
  return gameId && playerId ? (
    <>
      <DrawPile gameId={gameId} playerId={playerId} />
      <DiscardPile />
      <div style={{ position: "absolute", bottom: 0, height: 260, width: "80%", left: "10%", backgroundColor: "#FF000033" }}>
        <PlayerHand gameId={gameId} playerId={playerId} />
      </div>
    </>
  ) : <>Loading...</>;
}