import { Rank, Suit } from "./generated/globalTypes";
import "./App.css";

export interface CardProps {
    id: string
    rank: Rank
    suit: Suit
}

function toRankDisplay(rank: Rank, isJoker: boolean) {
    let result: string;
    if (isJoker) {
        result = "R";
    } else switch (rank) {
        case Rank.TWO:
            result = "2";
            break;
        case Rank.THREE:
            result = "3";
            break;
        case Rank.FOUR:
            result = "4";
            break;
        case Rank.FIVE:
            result = "5";
            break;
        case Rank.SIX:
            result = "6";
            break;
        case Rank.SEVEN:
            result = "7";
            break;
        case Rank.EIGHT:
            result = "8";
            break;
        case Rank.NINE:
            result = "9";
            break;
        case Rank.TEN:
            result = "10";
            break;
        case Rank.JACK:
            result = "J";
            break;
        case Rank.QUEEN:
            result = "Q";
            break;
        case Rank.KING:
            result = "K";
            break;
        case Rank.ACE:
            result = "A";
            break;
        default:
            result = "Er";
            break;
    }
    return result;
}

export function toZIndex(rank: Rank, isJoker: boolean) {
    let result: number;
    if (isJoker) {
        result = 0;
    } else switch (rank) {
        case Rank.TWO:
            result = 13;
            break;
        case Rank.THREE:
            result = 12;
            break;
        case Rank.FOUR:
            result = 11;
            break;
        case Rank.FIVE:
            result = 10;
            break;
        case Rank.SIX:
            result = 9;
            break;
        case Rank.SEVEN:
            result = 8;
            break;
        case Rank.EIGHT:
            result = 7;
            break;
        case Rank.NINE:
            result = 6;
            break;
        case Rank.TEN:
            result = 5;
            break;
        case Rank.JACK:
            result = 4;
            break;
        case Rank.QUEEN:
            result = 3;
            break;
        case Rank.KING:
            result = 2;
            break;
        case Rank.ACE:
            result = 1;
            break;
        default:
            result = -1;
            break;
    }
    return 100 + result * 10;
}

function toSuitDisplay(suit: Suit) {
    let result: string;
    switch (suit) {
        case Suit.CLUB:
            result = "♣";
            break;
        case Suit.DIAMOND:
            result = "♦";
            break;
        case Suit.HEART:
            result = "♥";
            break;
        case Suit.JOKER:
            result = "ᦔ";
            break;
        case Suit.SPADE:
            result = "♠";
            break;
        default:
            result = "Er";
            break;
    }
    return result;
}

export function Card({ id, rank, suit, cardOver }: CardProps & { cardOver?: boolean }) {
    let color;
    switch (suit) {
        case Suit.HEART:
        case Suit.DIAMOND:
            color = "red";
            break;
        case Suit.JOKER:
            color = "blue"
            break;
        default:
            color = "black";
    }

    return (
        // margin={`{'left': '${i += 10}px', 'top': '0px'}`}
        <div className="card" id={id} style={{ color, backgroundColor: cardOver ? "yellow" : "white" }}>
            <span style={{ fontSize: "150%", position: "fixed", margin: 4 }}>{toRankDisplay(rank, suit === Suit.JOKER)}</span>
            <span style={{ fontSize: "150%", position: "fixed", margin: 4, right: 0 }}>{toSuitDisplay(suit)}</span>
            <div><br /><br /></div>
            <div style={{ fontSize: "400%", color, textAlign: "center" }}>{toSuitDisplay(suit)}</div>
        </div>
    );
}