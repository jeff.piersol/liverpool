import { PropsWithChildren } from 'react';
import './App.css';

export function DiscardPile(props: PropsWithChildren<Object>) {
    return (
        <>
            <div style={{ position: "absolute", top: 50, left: 250 }}>
                {props.children}
            </div>
        </>
    );
}