import Draggable from "react-draggable";
import { gql, useMutation, useQuery } from "@apollo/client";
import { Card, CardProps, toZIndex } from "./Card";
import { hand } from "./generated/hand";
import { Discard, DiscardVariables } from "./generated/Discard";
import { HAS_CURRENT_PLAYER_DRAWN } from "./DrawPile";
import { GameViewId } from "./Game";
import { useState } from "react";
import { MouseEventHandler } from "react";

export const HAND = gql`
query hand($gameId: ID!, $playerId: ID!) {
    game(id: $gameId) {
        hand(player: $playerId) {
            id
            deck
            suit
            rank
        }
    }
}
`;

export const DISCARD = gql`
mutation Discard($gameId: ID!, $playerId: ID!, $cardId: ID!) {
    discard(game: $gameId, player: $playerId, card: $cardId)
}
`

function DraggableCard({ card, initXOffset }: { card: CardProps, initXOffset: number }) {
    const [zIndex, setZIndex] = useState<number | undefined>(undefined);
    const [dragging, setDragging] = useState(false);
    const [cardOver, setCardOver] = useState(false);
    return (<Draggable defaultPosition={{ x: initXOffset, y: 0 }}
        onStart={() => setDragging(true)}
        onStop={(evt, data) => {
            setDragging(false);
            // const discardResult = discard({ variables: { gameId, playerId, cardId: data.node.children[0].id } });
        }}>
        <div style={{ bottom: 0, position: "absolute", zIndex: dragging ? 10000 : zIndex }}
            onMouseOver={() => setCardOver(true)}
            onMouseOut={() => setCardOver(false)}>
            <Card key={card.id} id={card.id} rank={card.rank} suit={card.suit} cardOver={cardOver} />
        </div>
    </Draggable >);
}

/**
 * Component that renders all of the cards in a player's hand.
 * @param playerView the ID of the player's hand
 * @returns 
 */
export function PlayerHand(playerView: GameViewId) {
    let i = 0;
    const { data: playerQueryResult } = useQuery<hand>(HAND, { variables: playerView });
    const [discard] = useMutation<Discard, DiscardVariables>(DISCARD, {
        refetchQueries: [ // TODO: actually update cache efficiently
            { query: HAND, variables: playerView },
            { query: HAS_CURRENT_PLAYER_DRAWN, variables: { gameId: playerView.gameId } }
        ]
    });
    const cardClickGenerator = (cardId: string) => {
        const cardClick: MouseEventHandler<HTMLDivElement> = e => {
            if (e.detail != 2) // Only capture double clicks
                return;
            const result = discard({
                variables: {
                    gameId: playerView.gameId,
                    playerId: playerView.playerId,
                    cardId
                }
            })
            result.then(val => { console.log("Discarded? " + val.data?.discard) })
        }
        return cardClick
    }
    return (<>
        {playerQueryResult?.game?.hand.map(card => {
            return <div onClick={cardClickGenerator(card.id)}><DraggableCard card={card} initXOffset={i += 20} /></div>
        })}
    </>);
}