import React, { useState } from 'react';
import { Box, BoxProps, Button, Collapsible, Grommet, Heading, Layer, ResponsiveContext, ThemeType } from 'grommet';
import { FormClose, Notification } from 'grommet-icons';
import { ApolloClient, ApolloProvider, from, gql, HttpLink, InMemoryCache, useQuery } from '@apollo/client';
import { onError } from "@apollo/client/link/error";
import { Game } from './Game';

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors)
    graphQLErrors.forEach(({ message, locations, path }) =>
      console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
      ),
    );

  if (networkError) console.log(`[Network error]: ${networkError}`);
});

const client = new ApolloClient({
  link: from([new HttpLink({ uri: "http://localhost:8080/graphql" }), errorLink]),
  cache: new InMemoryCache()
});

const theme: ThemeType = {
  global: {
    colors: {
      brand: '#228BE6'
    },
    font: {
      // family: 'Roboto',
      size: '18px',
      height: '20px',
    },
  },
};

const AppBar = (props: BoxProps & React.ClassAttributes<HTMLDivElement> & React.HTMLAttributes<HTMLDivElement>) => (
  <Box
    tag='header'
    direction='row'
    align='center'
    justify='between'
    background='brand'
    pad={{ left: 'medium', right: 'small', vertical: 'small' }}
    elevation='medium'
    style={{ zIndex: 1 }}
    {...props}
  />
);

function App() {
  const [showSidebar, setShowSidebar] = useState(false);
  const [playerId, setPlayerId] = useState<string>();
  return (
    <>
      <input onChange={evt => setPlayerId(evt.target.value)} size={60} />
      <ApolloProvider client={client}>
        <Game playerId={playerId} />
      </ApolloProvider>
    </>
  );
}

export default App;
