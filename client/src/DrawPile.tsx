import './App.css';
import { gql, useMutation, useQuery } from "@apollo/client";
import { Draw, DrawVariables } from './generated/draw';
import { hasCurrentPlayerDrawn } from './generated/hasCurrentPlayerDrawn';
import { HAND } from './PlayerHand';
import { GameViewId } from './Game';

export const HAS_CURRENT_PLAYER_DRAWN = gql`
query hasCurrentPlayerDrawn($gameId: ID!) {
    game(id: $gameId) {
        currentPlayer {
            id
        }
        hasCurrentPlayerDrawn
    }
}
`;

const DRAW = gql`
mutation Draw($gameId: ID!, $playerId: ID!, $fromDiscard: Boolean) {
    draw(game: $gameId, player: $playerId, fromDiscard: $fromDiscard) {
        id
        deck
        suit
        rank
    }
}
`;

export function DrawPile({ gameId, playerId }: GameViewId) {
    const { data: curPlayDrawnQuery } = useQuery<hasCurrentPlayerDrawn>(HAS_CURRENT_PLAYER_DRAWN, { variables: { gameId } });
    const [drawCard] = useMutation<Draw, DrawVariables>(DRAW, {
        refetchQueries: [ // TODO: actually update cache efficiently
            { query: HAND, variables: { gameId, playerId } },
            { query: HAS_CURRENT_PLAYER_DRAWN, variables: { gameId } }
        ]
    });

    const drawProhibited = curPlayDrawnQuery?.game?.currentPlayer.id !== playerId || curPlayDrawnQuery?.game?.hasCurrentPlayerDrawn;
    return (
        <div className={`card draw-card${drawProhibited ? '-disabled' : ''}`}
            style={{ position: "absolute", top: 50, left: 50 }}
            onClick={evt => {
                const drawResult = drawCard({ variables: { gameId, playerId, fromDiscard: false } });
            }}
        />
    );
}